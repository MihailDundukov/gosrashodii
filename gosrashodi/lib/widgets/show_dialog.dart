import 'package:flutter/material.dart';
import 'package:get/get.dart';

showMyDialog(BuildContext context, Widget content) {
  return showDialog(
    context: context,
    builder: (ctx) => AlertDialog(
        titlePadding: const EdgeInsets.only(top: 16),
        contentPadding: const EdgeInsets.all(16),
        backgroundColor: const Color.fromRGBO(30, 38, 69, 1),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(16))),
        title: Container(
          width: Get.width,
          alignment: Alignment.topCenter,
          child: const Text(
            'Справка',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 24,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
            ),
          ),
        ),
        content: content),
  );
}
