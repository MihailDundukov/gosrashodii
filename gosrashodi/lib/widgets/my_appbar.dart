import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:gosrashodi/helpers/enums.dart';

AppBar myAppBar(List<Widget>? action, String title, BuildContext context,
    TabBar? bottom, Widget? otherBottom) {
  final controller = Get.find<DateController>();
  final navController = Get.find<NavigatorController>();
  final contractsController = Get.find<MainContractsController>();
  final grantsController = Get.find<MainGrantsController>();
  final subsidiesController = Get.find<MainSubsidyController>();

  return AppBar(
    backgroundColor: const Color.fromRGBO(30, 38, 69, 1),
    actions: action,
    bottom: bottom == null && otherBottom == null
        ? PreferredSize(
            preferredSize: Size(Get.width, 20),
            child: Container(
              alignment: Alignment.topCenter,
              padding: const EdgeInsets.symmetric(vertical: 4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Obx(
                    () => GestureDetector(
                      onTap: () {
                        showModalBottomSheet(
                            context: context,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16),
                            )),
                            builder: (context) {
                              return Column(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      if (navController.type ==
                                          MyScreen.contracts) {
                                        contractsController.reload();
                                      } else if (navController.type ==
                                          MyScreen.grants) {
                                        grantsController.reload();
                                      } else if (navController.type ==
                                          MyScreen.fedSubs) {
                                        subsidiesController.reload();
                                      }
                                      Get.back();
                                    },
                                    child: Container(
                                      margin: const EdgeInsets.symmetric(
                                              vertical: 8, horizontal: 32)
                                          .copyWith(bottom: 0),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12, horizontal: 32),
                                      alignment: Alignment.center,
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(16)),
                                        color:
                                            Color.fromARGB(255, 197, 197, 197),
                                      ),
                                      child: const Text(
                                        'Выбрать',
                                        style: TextStyle(
                                          fontFamily: 'Ubuntu',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: CupertinoDatePicker(
                                      mode: CupertinoDatePickerMode.date,
                                      initialDateTime: DateTime.utc(
                                        controller.firstPickerDate[0],
                                        controller.firstPickerDate[1],
                                        controller.firstPickerDate[2],
                                      ),
                                      minimumYear: 2010,
                                      maximumDate: DateTime.now(),
                                      onDateTimeChanged: (newDate) {
                                        controller.setFirstDate(newDate);
                                      },
                                    ),
                                  ),
                                ],
                              );
                            });
                      },
                      child: Row(
                        children: [
                          const Icon(
                            Icons.keyboard_arrow_down_rounded,
                            color: Color.fromRGBO(245, 245, 245, 1),
                          ),
                          Text(
                            controller.textFirstDate.value,
                            style: const TextStyle(
                              fontFamily: 'Ubuntu',
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Color.fromRGBO(245, 245, 245, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  const Text(
                    '-',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Obx(
                    () => GestureDetector(
                      onTap: () {
                        showModalBottomSheet(
                            context: context,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16),
                            )),
                            builder: (context) {
                              return Column(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      if (navController.type ==
                                          MyScreen.contracts) {
                                        contractsController.reload();
                                      }
                                      if (navController.type ==
                                          MyScreen.grants) {
                                        grantsController.reload();
                                      }
                                      Get.back();
                                    },
                                    child: Container(
                                      margin: const EdgeInsets.symmetric(
                                              vertical: 8, horizontal: 32)
                                          .copyWith(bottom: 0),
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 12, horizontal: 32),
                                      alignment: Alignment.center,
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(16)),
                                        color:
                                            Color.fromARGB(255, 197, 197, 197),
                                      ),
                                      child: const Text(
                                        'Выбрать',
                                        style: TextStyle(
                                          fontFamily: 'Ubuntu',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: CupertinoDatePicker(
                                      mode: CupertinoDatePickerMode.date,
                                      initialDateTime: DateTime.utc(
                                        controller.secondPickerDate[0],
                                        controller.secondPickerDate[1],
                                        controller.secondPickerDate[2],
                                      ),
                                      minimumYear: 2010,
                                      maximumDate: DateTime.now(),
                                      onDateTimeChanged: (newDate) {
                                        controller.setSecondDate(newDate);
                                      },
                                    ),
                                  ),
                                ],
                              );
                            });
                      },
                      child: Row(
                        children: [
                          Text(
                            controller.textSecondDate.value,
                            style: const TextStyle(
                              fontFamily: 'Ubuntu',
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Color.fromRGBO(245, 245, 245, 1),
                            ),
                          ),
                          const Icon(
                            Icons.keyboard_arrow_down_rounded,
                            color: Color.fromRGBO(245, 245, 245, 1),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        : bottom ??
            PreferredSize(
              preferredSize: Size(Get.width, 20),
              child: otherBottom!,
            ),
    title: Text(
      title,
      style: const TextStyle(
        color: Color.fromRGBO(245, 245, 245, 1),
        fontFamily: 'Ubuntu',
        fontSize: 24,
        fontWeight: FontWeight.w600,
      ),
    ),
  );
}
