import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(
          child: Text(
            'Не удалось получить данные',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(224, 54, 105, 1),
            ),
          ),
        ),
        const SizedBox(
          height: 32,
        ),
        Image.asset(
          'assets/images/warning.png',
          height: 82,
          width: 82,
        ),
      ],
    );
  }
}
