import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/load_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      enableLog: false,
      defaultTransition: Transition.rightToLeft,
      transitionDuration: const Duration(milliseconds: 100),
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: LoadScreen(),
    );
  }
}

