import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';

class GrantDetail extends StatelessWidget {
  final Map<String, dynamic> data;
  const GrantDetail({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8),
          ],
        ),
      ),
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar(
            null,
            data['name_project'] ?? 'Проект',
            context,
            const TabBar(
              indicatorWeight: 5,
              indicatorColor: Colors.black,
              tabs: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Инфо',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Описание',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
              ],
            ),
            null,
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            child: TabBarView(
              children: [
                info(),
                description(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget description() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Text(
          data['description'],
          style: const TextStyle(
            fontFamily: 'Ubuntu',
            fontSize: 18,
            fontWeight: FontWeight.w800,
            color: Color.fromRGBO(245, 245, 245, 1),
            letterSpacing: 1,
          ),
        ),
      ),
    );
  }

  Widget info() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'ОГРН',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Text(
              data['OGRN'] ?? 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            const Text(
              'Компания:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['name_organization'] ?? 'НД',
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w800,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Адрес',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['address'] ?? 'НД',
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w800,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 18,
            ),
            const Text(
              'Заключен оператором',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['operator'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            RichText(
              textScaleFactor: Get.textScaleFactor,
              text: TextSpan(
                children: [
                  const TextSpan(
                    text: 'Дата: ',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 0.3),
                    ),
                  ),
                  TextSpan(
                    text: getDate(data['filing_date']),
                    style: const TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 18,
                      fontWeight: FontWeight.w800,
                      color: Color.fromRGBO(245, 245, 245, 1),
                      letterSpacing: 1,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getDate(String date) {
    List<String> listDate = date.substring(0, 10).split('-');
    return listDate[2] + '.' + listDate[1] + '.' + listDate[0];
  }
}
