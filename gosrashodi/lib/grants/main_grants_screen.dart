// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/grants/grant_detail.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:gosrashodi/widgets/error.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';
import 'package:gosrashodi/widgets/show_dialog.dart';

class MainGrantsScreen extends StatelessWidget {
  MainGrantsScreen({Key? key}) : super(key: key);
  final controller = Get.find<MainGrantsController>();
  RxBool tapped = false.obs;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8)
          ],
        ),
      ),
      child: Obx(
        () => Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar([
            GestureDetector(
              onTap: () {
                showMyDialog(
                  context,
                  IntrinsicHeight(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'В целях снижения нагрузки на сервер максимальное число объектов ограничено 500. Приносим свои извинения за предоставленные неудобства!',
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(245, 245, 245, 1),
                            letterSpacing: 1,
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Wrap(
                          children: const [
                            Text(
                              'Подсказка: ',
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(90, 90, 100, 1),
                              ),
                            ),
                            Text(
                              'Для более делального просмотра объектов воспользуйтесь настройкой отчетного периода в шапке экрана.',
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(245, 245, 245, 1),
                                letterSpacing: 1,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
              child: Container(
                height: 30,
                width: 30,
                margin: const EdgeInsets.only(right: 16),
                child: const Icon(
                  Icons.info_outline_rounded,
                  size: 28,
                  color: Color.fromRGBO(245, 245, 245, 1),
                ),
              ),
            ),
          ], 'Гос. гранты', context, null, null),
          body: controller.data.isNotEmpty
              ? getBody()
              : FutureBuilder<dynamic>(
                  future: controller.getData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return controller.data.isEmpty
                          ? const Center(
                              child: Text(
                                'Нет данных',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(224, 54, 105, 1),
                                ),
                              ),
                            )
                          : getBody();
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorScreen());
                    } else {
                      return const Center(
                        child: CupertinoActivityIndicator(
                          color: Color.fromRGBO(245, 245, 245, 1),
                          radius: 20,
                        ),
                      );
                    }
                  },
                ),
        ),
      ),
    );
  }

  Widget loadMoreWidget() {
    return GestureDetector(
      onTap: () {
        tapped.value = true;
      },
      child: Obx(
        () => Container(
          height: 70,
          decoration: const BoxDecoration(
            color: Color.fromRGBO(30, 38, 69, 1),
            borderRadius: BorderRadius.all(Radius.circular(16)),
          ),
          alignment: Alignment.center,
          child: tapped.value
              ? FutureBuilder<dynamic>(
                  future: controller.getData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return const SizedBox();
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorScreen());
                    } else {
                      return const Center(
                        child: CupertinoActivityIndicator(
                          color: Color.fromRGBO(245, 245, 245, 1),
                          radius: 20,
                        ),
                      );
                    }
                  },
                )
              : const Text(
                  'Загрузить еще',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                ),
        ),
      ),
    );
  }

  Widget getBody() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView.builder(
        itemCount: controller.canBeMore.value
            ? controller.data.length + 1
            : controller.data.length,
        padding: EdgeInsets.zero,
        itemBuilder: (cts, ind) => ind != controller.data.length
            ? GrantCard(index: ind)
            : loadMoreWidget(),
      ),
    );
  }
}

class GrantCard extends StatelessWidget {
  GrantCard({Key? key, required this.index}) : super(key: key);
  final controller = Get.find<MainGrantsController>();
  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(
          popGesture: true,
          () => GrantDetail(
            data: controller.data[index],
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(16),
        margin: const EdgeInsets.only(bottom: 16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              controller.data[index]["name_project"],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              controller.data[index]['name_organization'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.5),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Выделено денег: ',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              controller.data[index]['price'] == null
                  ? 'Выделено лично по завершению'
                  : parsePrice(controller.data[index]['price'].toString()),
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            Text(
              'Заключен в ' + controller.data[index]['year'] + ' году.',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.5),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

String parsePrice(String price) {
  String ed = '';
  double dPrice = double.parse(price);
  if (dPrice >= 1000000000) {
    dPrice /= 1000000000;
    ed = 'млрд. RUB';
  } else if (dPrice >= 1000000) {
    dPrice /= 1000000;
    ed = 'млн. RUB';
  } else if (dPrice >= 1000) {
    dPrice /= 1000;
    ed = 'тыс. RUB';
  }
  return dPrice.toStringAsFixed(1) + ' ' + ed;
}
