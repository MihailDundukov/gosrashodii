import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/contracts/main_contracts_screen.dart';
import 'package:gosrashodi/fed_subsidy/main_subsidy_screen.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:gosrashodi/helpers/enums.dart';
import 'package:gosrashodi/suppliers/main_suppliers_screen.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';
import 'package:gosrashodi/widgets/show_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

import 'grants/main_grants_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  final String _url = 'https://clearspending.ru';

  void _launchURL() async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8)
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: myAppBar([
          GestureDetector(
            onTap: () {
              showMyDialog(
                context,
                IntrinsicHeight(
                  child: Column(
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        margin: const EdgeInsets.only(top: 8),
                        padding: const EdgeInsets.all(12),
                        child: Image.asset(
                          'assets/images/logoImg.png',
                          height: 120,
                          width: 120,
                        ),
                      ),
                      const SizedBox(height: 16),
                      const Text(
                        'Госпрограммы',
                        style: TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      const SizedBox(
                        child: Text(
                          'Данное приложение было создано для наиболее удобного отслеживания трат бюджета страны властями.\n\nЗдесь пердставлена только оффициальная информация с гос. серверов, всязи с чем у Вас нет необходимости им не доверять.',
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(245, 245, 245, 1),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      const Text(
                        'Сайт:',
                        style: TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      GestureDetector(
                        onTap: () {
                          _launchURL();
                        },
                        child: Text(
                          'ГосЗатраты.ру',
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Colors.blue[800],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
            child: Container(
              height: 30,
              width: 30,
              margin: const EdgeInsets.only(right: 16),
              child: const Icon(
                Icons.info_outline_rounded,
                size: 28,
                color: Color.fromRGBO(245, 245, 245, 1),
              ),
            ),
          ),
        ], 'Госрасходы', context, null, null),
        body: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: 4,
            itemBuilder: (ctx, ind) => HomeCard(
              index: ind,
            ),
          ),
        ),
      ),
    );
  }
}

class HomeCard extends StatelessWidget {
  final int index;
  HomeCard({Key? key, required this.index}) : super(key: key);
  final controller = Get.find<NavigatorController>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (index == 0) {
          controller.type = MyScreen.contracts;
          Get.to(
            popGesture: true,
            () => MainContractsScreen(),
            routeName: '/MainContractsScreen',
          )!
              .then((value) {
            controller.type = MyScreen.start;
          });
        } else if (index == 1) {
          controller.type = MyScreen.grants;
          Get.to(
            popGesture: true,
            () => MainGrantsScreen(),
            routeName: '/MainGrantsScreen',
          )!
              .then((value) {
            controller.type = MyScreen.start;
          });
        } else if (index == 2) {
          controller.type = MyScreen.suppliers;
          Get.to(
                  popGesture: true,
                  () => MainSuppliersScreen(),
                  routeName: '/MainSuppliersScreen')!
              .then((value) {
            controller.type = MyScreen.start;
          });
        } else if (index == 3) {
          controller.type = MyScreen.fedSubs;
          Get.to(
                  popGesture: true,
                  () => MainSubsidyScreen(),
                  routeName: '/MainSubsidyScreen')!
              .then((value) {
            controller.type = MyScreen.start;
          });
        }
      },
      child: Container(
        margin: const EdgeInsets.all(16).copyWith(top: 0, left: 22),
        padding: const EdgeInsets.all(8),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(30, 65, 200, 1),
              blurRadius: 5,
            )
          ],
        ),
        alignment: Alignment.center,
        height: 80,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Transform.translate(
                  offset: const Offset(-16, -14),
                  child: Container(
                    height: 40,
                    width: 60,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                      color: Color.fromRGBO(30, 38, 150, 1),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      getRimNum(index),
                      style: const TextStyle(
                        fontFamily: 'Ubuntu',
                        fontSize: 20,
                        fontWeight: FontWeight.w800,
                        color: Color.fromRGBO(245, 245, 245, 1),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  getTitle(index),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                showMyDialog(context, getContent(index));
              },
              child: Container(
                height: 40,
                width: 40,
                alignment: Alignment.centerRight,
                child: const Icon(
                  Icons.info_outlined,
                  size: 26,
                  color: Color.fromRGBO(60, 65, 90, 1),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

String getRimNum(int index) {
  switch (index) {
    case 0:
      return 'I';
    case 1:
      return 'II';
    case 2:
      return 'III';
    case 3:
      return 'IV';
    case 4:
      return 'V';
    case 5:
      return 'VI';
    default:
      return '';
  }
}

String getTitle(int index) {
  switch (index) {
    case 0:
      return 'Контракты';
    case 1:
      return 'Гранты';

    case 2:
      return 'Поставщики';
    case 3:
      return 'Фед. субсидии';
    default:
      return '';
  }
}

Widget getContent(int index) {
  switch (index) {
    case 0:
      return infoWidget('Описание полей для работы с контрактами');
    case 1:
      return infoWidget('Описание полей для работы с грантами');
    case 2:
      return infoWidget('Описание полей для работы с заказчиками');
    case 3:
      return infoWidget('Описание полей для работы с поставщиками');
    case 4:
      return infoWidget('Описание полей для работы с топами');
    case 5:
      return infoWidget('Описание полей для работы с федеральными субсидиями');
    default:
      return infoWidget('');
  }
}

Widget infoWidget(String content) {
  return SizedBox(
    child: Text(
      content,
      style: const TextStyle(
        fontFamily: 'Ubuntu',
        fontSize: 18,
        fontWeight: FontWeight.w400,
        color: Color.fromRGBO(245, 245, 245, 1),
      ),
    ),
  );
}
