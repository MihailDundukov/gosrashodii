import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';

class SubsidyDetail extends StatelessWidget {
  final Map<String, dynamic> data;
  const SubsidyDetail({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8),
          ],
        ),
      ),
      child: DefaultTabController(
        length: 4,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar(
            null,
            data['plansNormalized'][0]['purpose'] ?? 'Субсидия',
            context,
            const TabBar(
              isScrollable: true,
              indicatorWeight: 5,
              indicatorColor: Colors.black,
              tabs: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Инфо',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Получатель',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'НПА',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'ГРБС',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
              ],
            ),
            null,
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            child: TabBarView(
              children: [
                info(),
                receiver(),
                npa(),
                grbs(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget info() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              child: Text(
                data['plansNormalized'][0]['purpose'] ?? 'Субсидия',
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Код ГРБС',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 0.3),
                  ),
                ),
                Text(
                  data['plansNormalized'][0]['grbsCode'].toString(),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 1),
                    letterSpacing: 1,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              'Тип:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['info']['mfName'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Заключено:',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 0.3),
                  ),
                ),
                Text(
                  parseDate(
                    data['info']['dateAgreem'].toString(),
                  ),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 1),
                    letterSpacing: 1,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Последнее\nобновление',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 0.3),
                  ),
                ),
                Text(
                  parseDate(
                    data['info']['dateUpdate'].toString(),
                  ),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 1),
                    letterSpacing: 1,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget receiver() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              child: Text(
                data['receiver'][0]['shortName'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'ИНН:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['receiver'][0]['inn'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              'КПП:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['receiver'][0]['kpp'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              'ОГРН:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['receiver'][0]['ogrn'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Компания внесена в реестр:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              parseDate(
                data['receiver'][0]['dateAccount'],
              ),
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Text(
              data['receiver'][0]['localAddress']['localName'] +
                  ', ' +
                  data['receiver'][0]['localAddress']['regionName'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget npa() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Тип:',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 0.3),
                  ),
                ),
                Text(
                  data['npa'][0]['npaKind'],
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            SizedBox(
              child: Text(
                data['npa'][0]['npaName'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Подписано:',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 0.3),
                  ),
                ),
                Text(
                  parseDate(data['npa'][0]['acceptDate'].toString()),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget grbs() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              child: Text(
                data['grbs']['shortName'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Отдел:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['grbs']['tofkname'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'ИНН:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['grbs']['inn'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              'КПП:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['grbs']['kpp'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            const Text(
              'ОГРН:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['grbs']['ogrn'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            SizedBox(
              child: Text(
                data['grbs']['location'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  String parseDate(String date) {
    return date.substring(0, 10).replaceAll('-', '.');
  }
}
