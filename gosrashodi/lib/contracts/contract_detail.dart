import 'package:flutter/material.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';

class ContractDetail extends StatelessWidget {
  final Map<String, dynamic> data;
  const ContractDetail({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8),
          ],
        ),
      ),
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar(
            null,
            data['customer']['shortName'] ?? 'Заказчик',
            context,
            const TabBar(
              indicatorWeight: 5,
              indicatorColor: Colors.black,
              tabs: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Инфо',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Заказы',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
              ],
            ),
            null,
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 16, left: 16),
            child: TabBarView(
              children: [
                info(),
                products(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget products() {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemCount: data['products'].length,
      itemBuilder: (context, index) => productCard(index),
    );
  }

  Widget productCard(int index) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16, right: 16),
      padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(16),
        ),
        color: Color.fromRGBO(30, 38, 69, 1),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(15, 15, 15, 1),
            blurRadius: 5,
          )
        ],
      ),
      child: SizedBox(
        child: Text(
          data['products'][index]['name'],
          style: const TextStyle(
            fontFamily: 'Ubuntu',
            fontSize: 16,
            fontWeight: FontWeight.w800,
            color: Color.fromRGBO(245, 245, 245, 1),
            letterSpacing: 1,
          ),
        ),
      ),
    );
  }

  Widget info() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'КПП',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Text(
              data['customer']['kpp'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            const Text(
              'ОКАТО',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Text(
              data['customer']['OKATO'] ?? 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            const Text(
              'ОГРН',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Text(
              data['customer']['OGRN'] ?? 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            const Text(
              'ИКО',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Text(
              data['customer']['iko'] ?? 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Компания:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['customer']['fullName'] ?? 'НД',
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w800,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Адрес',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['customer']['postalAddress'] ?? 'НД',
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w800,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            Text(
              data['placer'] != null
                  ? data['placer']['mainInfo']['email']
                  : 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 16,
                fontWeight: FontWeight.w800,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
