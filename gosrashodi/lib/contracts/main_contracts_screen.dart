// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/contracts/contract_detail.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:gosrashodi/widgets/error.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';
import 'package:gosrashodi/widgets/show_dialog.dart';

class MainContractsScreen extends StatelessWidget {
  final MainContractsController controller =
      Get.find<MainContractsController>();
  RxBool tapped = false.obs;
  MainContractsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8),
          ],
        ),
      ),
      child: Obx(
        () => Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar([
            GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    backgroundColor: const Color.fromRGBO(10, 22, 74, 1),
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(16),
                        topRight: Radius.circular(16),
                      ),
                    ),
                    builder: (ctx) => Obx(
                      () => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.topCenter,
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            decoration: const BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  width: 1,
                                  color: Color.fromRGBO(245, 245, 245, 0.4),
                                ),
                              ),
                            ),
                            child: const Text(
                              'Фильтры',
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                fontSize: 22,
                                fontWeight: FontWeight.w600,
                                color: Color.fromRGBO(245, 245, 245, 1),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (controller.priceMax.value) {
                                controller.priceMax.value = false;
                              } else {
                                controller.priceMax.value = true;
                                controller.priceMin.value = false;
                                controller.countMax.value = false;
                                controller.countMin.value = false;
                              }
                              controller.sortData();
                            },
                            child: sortButton(
                              Icons.arrow_upward_rounded,
                              'Сумма',
                              controller.priceMax.value,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (controller.priceMin.value) {
                                controller.priceMin.value = false;
                              } else {
                                controller.priceMin.value = true;
                                controller.priceMax.value = false;
                                controller.countMax.value = false;
                                controller.countMin.value = false;
                              }
                              controller.sortData();
                            },
                            child: sortButton(
                              Icons.arrow_downward_rounded,
                              'Сумма',
                              controller.priceMin.value,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (controller.countMax.value) {
                                controller.countMax.value = false;
                              } else {
                                controller.priceMin.value = false;
                                controller.priceMax.value = false;
                                controller.countMax.value = true;
                                controller.countMin.value = false;
                              }
                              controller.sortData();
                            },
                            child: sortButton(
                              Icons.arrow_upward_rounded,
                              'Кол-во контрактов',
                              controller.countMax.value,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              if (controller.countMin.value) {
                                controller.countMin.value = false;
                              } else {
                                controller.priceMin.value = false;
                                controller.priceMax.value = false;
                                controller.countMax.value = false;
                                controller.countMin.value = true;
                              }
                              controller.sortData();
                            },
                            child: sortButton(
                              Icons.arrow_downward_rounded,
                              'Кол-во контрактов',
                              controller.countMin.value,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                child: const Icon(
                  Icons.sort,
                  size: 28,
                  color: Color.fromRGBO(245, 245, 245, 1),
                )),
            const SizedBox(
              width: 8,
            ),
            GestureDetector(
              onTap: () {
                showMyDialog(
                  context,
                  IntrinsicHeight(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'В целях снижения нагрузки на сервер максимальное число объектов ограничено 500. Приносим свои извинения за предоставленные неудобства!',
                          style: TextStyle(
                            fontFamily: 'Ubuntu',
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(245, 245, 245, 1),
                            letterSpacing: 1,
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Wrap(
                          children: const [
                            Text(
                              'Подсказка: ',
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(90, 90, 100, 1),
                              ),
                            ),
                            Text(
                              'Для более делального просмотра объектов воспользуйтесь настройкой отчетного периода в шапке экрана.',
                              style: TextStyle(
                                fontFamily: 'Ubuntu',
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Color.fromRGBO(245, 245, 245, 1),
                                letterSpacing: 1,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
              child: Container(
                height: 30,
                width: 30,
                margin: const EdgeInsets.only(right: 16),
                child: const Icon(
                  Icons.info_outline_rounded,
                  size: 28,
                  color: Color.fromRGBO(245, 245, 245, 1),
                ),
              ),
            ),
          ], 'Гос. контракты', context, null, null),
          body: controller.data.isEmpty
              ? FutureBuilder<dynamic>(
                  future: controller.getData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return controller.data.isEmpty
                          ? const Center(
                              child: Text(
                                'Нет данных',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(224, 54, 105, 1),
                                ),
                              ),
                            )
                          : getBody();
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorScreen());
                    } else {
                      return const Center(
                        child: CupertinoActivityIndicator(
                          color: Color.fromRGBO(245, 245, 245, 1),
                          radius: 20,
                        ),
                      );
                    }
                  },
                )
              : getBody(),
        ),
      ),
    );
  }

  Widget sortButton(IconData icon, String title, bool selected) {
    return Container(
      padding: const EdgeInsets.fromLTRB(8, 12, 8, 12),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Color.fromRGBO(245, 245, 245, 0.6),
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Icon(
                icon,
                size: 28,
                color: const Color.fromRGBO(245, 245, 245, 1),
              ),
              const SizedBox(
                width: 32,
              ),
              Text(
                title,
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                ),
              ),
            ],
          ),
          Opacity(
            opacity: selected ? 1 : 0,
            child: Container(
              height: 28,
              width: 28,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Color.fromRGBO(1, 130, 40, 1),
              ),
              child: const Icon(
                Icons.done,
                size: 24,
                color: Color.fromRGBO(245, 245, 245, 1),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget loadMoreWidget() {
    return GestureDetector(
      onTap: () {
        tapped.value = true;
      },
      child: Obx(
        () => Container(
          height: 70,
          decoration: const BoxDecoration(
            color: Color.fromRGBO(30, 38, 69, 1),
            borderRadius: BorderRadius.all(Radius.circular(16)),
          ),
          alignment: Alignment.center,
          child: tapped.value
              ? FutureBuilder<dynamic>(
                  future: controller.getData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return const SizedBox();
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorScreen());
                    } else {
                      return const Center(
                        child: CupertinoActivityIndicator(
                          color: Color.fromRGBO(245, 245, 245, 1),
                          radius: 20,
                        ),
                      );
                    }
                  },
                )
              : const Text(
                  'Загрузить еще',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                ),
        ),
      ),
    );
  }

  Widget getBody() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(16),
            margin: const EdgeInsets.only(bottom: 16),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(16),
              ),
              color: Color.fromRGBO(30, 38, 69, 1),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(15, 15, 15, 1),
                  blurRadius: 5,
                )
              ],
            ),
            child: Obx(
              () => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    child: Text(
                      'Средств на ${controller.data.length} контрактов ушло:',
                      style: const TextStyle(
                        fontFamily: 'Ubuntu',
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(245, 245, 245, 0.3),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        parsePrice(
                          controller.getAllPrice(),
                        ),
                        style: const TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(245, 245, 245, 1),
                          letterSpacing: 1,
                        ),
                      ),
                      const Text(
                        'RUB',
                        style: TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 26,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(245, 245, 245, 1),
                          letterSpacing: 1,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: controller.canBeMore.value
                  ? controller.data.length + 1
                  : controller.data.length,
              padding: EdgeInsets.zero,
              itemBuilder: (cts, ind) => ind != controller.data.length
                  ? ContractCard(index: ind)
                  : loadMoreWidget(),
            ),
          ),
        ],
      ),
    );
  }
}

class ContractCard extends StatelessWidget {
  ContractCard({Key? key, required this.index}) : super(key: key);
  final controller = Get.find<MainContractsController>();
  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(
          popGesture: true,
          () => ContractDetail(
            data: controller.data[index],
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(16),
        margin: const EdgeInsets.only(bottom: 16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              controller.data[index]['customer']['shortName'] ?? 'Без имени',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Количество контрактов:',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(245, 245, 245, 0.3),
                  ),
                ),
                Text(
                  controller.data[index]['products'].length.toString(),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 26,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(245, 245, 245, 1),
                    letterSpacing: 1,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'На общую сумму:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Text(
                  parsePrice(controller.data[index]['price'].toString()),
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(245, 245, 245, 1),
                    letterSpacing: 1,
                  ),
                ),
                const SizedBox(
                  width: 4,
                ),
                Text(
                  controller.data[index]['currency']['code'],
                  style: const TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 20,
                    fontWeight: FontWeight.w800,
                    color: Color.fromRGBO(245, 245, 245, 1),
                    letterSpacing: 1,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Сроки выполнения: ',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            controller.data[index]['execution'] != null
                ? Row(
                    children: [
                      Text(
                        controller.data[index]['execution']['startDate'] == null
                            ? '___'
                            : controller.data[index]['execution']['startDate']
                                        .toString()
                                        .length >
                                    10
                                ? controller.data[index]['execution']
                                        ['startDate']
                                    .toString()
                                    .substring(0, 10)
                                : controller.data[index]['execution']
                                        ['startDate']
                                    .toString(),
                        style: const TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(245, 245, 245, 1),
                          letterSpacing: 1,
                        ),
                      ),
                      const Text(
                        '  /  ',
                        style: TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(90, 90, 100, 1),
                        ),
                      ),
                      Text(
                        controller.data[index]['execution']['endDate'] == null
                            ? '___'
                            : controller.data[index]['execution']['endDate']
                                        .toString()
                                        .length >
                                    10
                                ? controller.data[index]['execution']['endDate']
                                    .toString()
                                    .substring(0, 10)
                                : controller.data[index]['execution']
                                    ['endDate'],
                        style: const TextStyle(
                          fontFamily: 'Ubuntu',
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Color.fromRGBO(245, 245, 245, 1),
                          letterSpacing: 1,
                        ),
                      )
                    ],
                  )
                : const Text(
                    'Бессрочное',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 22,
                      fontWeight: FontWeight.w600,
                      color: Color.fromRGBO(245, 245, 245, 1),
                      letterSpacing: 1,
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}

String parsePrice(String price) {
  String ed = '';
  double dPrice = double.parse(price);
  if (dPrice >= 1000000000) {
    dPrice /= 1000000000;
    ed = 'млрд.';
  } else if (dPrice >= 1000000) {
    dPrice /= 1000000;
    ed = 'млн.';
  } else if (dPrice >= 1000) {
    dPrice /= 1000;
    ed = 'тыс.';
  }
  return dPrice.toStringAsFixed(1) + ' ' + ed;
}
