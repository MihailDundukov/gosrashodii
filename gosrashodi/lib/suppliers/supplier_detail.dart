import 'package:flutter/material.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';

class SupplierDetail extends StatelessWidget {
  final Map<String, dynamic> data;
  const SupplierDetail({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8),
          ],
        ),
      ),
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar(
            null,
            data['organizationName'] ?? 'Поставщик',
            context,
            const TabBar(
              isScrollable: false,
              indicatorWeight: 5,
              indicatorColor: Colors.black,
              tabs: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Инфо',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Погодовой отчет',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Заказчики',
                    style: TextStyle(
                      fontFamily: 'Ubuntu',
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                ),
              ],
            ),
            null,
          ),
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            child: TabBarView(
              children: [
                info(),
                yearsData(),
                customers(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget info() {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
        padding: const EdgeInsets.all(16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              child: Text(
                data['organizationName'] ?? 'Поставщик',
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'КПП:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['kpp'] ?? 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            const Text(
              'ИНН:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              data['inn'] ?? 'НД',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Общая вырученная сумма:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              parsePrice(
                data['contractsTotalSum'].toString(),
              ),
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Фактический адрес:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              child: Text(
                data['factualAddress'],
                style: const TextStyle(
                  fontFamily: 'Ubuntu',
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(245, 245, 245, 1),
                  letterSpacing: 1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String parsePrice(String price) {
    String ed = '';
    double dPrice = double.parse(price);
    if (dPrice >= 1000000000) {
      dPrice /= 1000000000;
      ed = 'млрд.';
    } else if (dPrice >= 1000000) {
      dPrice /= 1000000;
      ed = 'млн.';
    } else if (dPrice >= 1000) {
      dPrice /= 1000;
      ed = 'тыс.';
    }
    return dPrice.toStringAsFixed(1) + ' ' + ed + ' RUB';
  }

  Widget yearsData() {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemCount: 13,
      shrinkWrap: true,
      itemBuilder: (context, index) => yearCard((2010 + index).toString()),
    );
  }

  Widget yearCard(String year) {
    return Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
      padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(16),
        ),
        color: Color.fromRGBO(30, 38, 69, 1),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(15, 15, 15, 1),
            blurRadius: 5,
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            year,
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 24,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          const Text(
            'Кол-во контрактов',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(245, 245, 245, 0.3),
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            data['contractsYearStats'][year] == null
                ? 'НД'
                : data['contractsYearStats'][year]['contractsCount'].toString(),
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          const Text(
            'Вырученная сумма',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(245, 245, 245, 0.3),
            ),
          ),
          const SizedBox(
            height: 4,
          ),
          Text(
            data['contractsYearStats'][year] == null
                ? 'НД'
                : data['contractsYearStats'][year]['contractsSum'].toString(),
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget customers() {
    return ListView.builder(
      padding: EdgeInsets.zero,
      itemCount: data['customers_list'].length,
      shrinkWrap: true,
      itemBuilder: (context, index) =>
          customerCard(data['customers_list'][index]),
    );
  }

  Widget customerCard(var custData) {
    return Container(
        margin: const EdgeInsets.only(bottom: 16, right: 16),
      padding: const EdgeInsets.all(16),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(16),
        ),
        color: Color.fromRGBO(30, 38, 69, 1),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(15, 15, 15, 1),
            blurRadius: 5,
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            child: Text(
              custData['fullName'],
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          const Text(
            'КПП:',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(245, 245, 245, 0.3),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            custData['kpp'],
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          const Text(
            'ИНН:',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(245, 245, 245, 0.3),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            custData['inn'],
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          const Text(
            'Всего контрактов:',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(245, 245, 245, 0.3),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            custData['contractsCount'].toString(),
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          const Text(
            'Затрачено средств:',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: Color.fromRGBO(245, 245, 245, 0.3),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            parsePrice(custData['contractsSum'].toString()),
            style: const TextStyle(
              fontFamily: 'Ubuntu',
              fontSize: 20,
              fontWeight: FontWeight.w600,
              color: Color.fromRGBO(245, 245, 245, 1),
              letterSpacing: 1,
            ),
          ),
        ],
      ),
    );
  }
}
