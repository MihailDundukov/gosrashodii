// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:gosrashodi/suppliers/supplier_detail.dart';
import 'package:gosrashodi/widgets/error.dart';
import 'package:gosrashodi/widgets/my_appbar.dart';

class MainSuppliersScreen extends StatelessWidget {
  MainSuppliersScreen({Key? key}) : super(key: key);
  final controller = Get.find<MainSuppliersController>();
  RxBool tapped = false.obs;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            Color.fromRGBO(20, 20, 20, 1),
            Color.fromRGBO(10, 22, 74, 0.8),
          ],
        ),
      ),
      child: Obx(
        () => Scaffold(
          backgroundColor: Colors.transparent,
          appBar: myAppBar(
              null,
              'Поставщики',
              context,
              null,
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const SizedBox(
                    width: 16,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 12),
                    child: Text(
                      'Регион: ',
                      style: TextStyle(
                        fontFamily: 'Ubuntu',
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(245, 245, 245, 1),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: TextField(
                      controller: controller.textController,
                      onSubmitted: (val) {
                        controller.region.value = val;
                        controller.reload();
                      },
                      style: const TextStyle(
                        fontFamily: 'Ubuntu',
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(224, 54, 105, 1),
                      ),
                    ),
                  ),
                ],
              )),
          body: controller.data.isEmpty
              ? FutureBuilder<dynamic>(
                  future: controller.getData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return controller.data.isEmpty
                          ? const Center(
                              child: Text(
                                'Нет данных',
                                style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w600,
                                  color: Color.fromRGBO(224, 54, 105, 1),
                                ),
                              ),
                            )
                          : getBody();
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorScreen());
                    } else {
                      return const Center(
                        child: CupertinoActivityIndicator(
                          color: Color.fromRGBO(245, 245, 245, 1),
                          radius: 20,
                        ),
                      );
                    }
                  },
                )
              : getBody(),
        ),
      ),
    );
  }

  Widget loadMoreWidget() {
    return GestureDetector(
      onTap: () {
        tapped.value = true;
      },
      child: Obx(
        () => Container(
          height: 70,
          decoration: const BoxDecoration(
            color: Color.fromRGBO(30, 38, 69, 1),
            borderRadius: BorderRadius.all(Radius.circular(16)),
          ),
          alignment: Alignment.center,
          child: tapped.value
              ? FutureBuilder<dynamic>(
                  future: controller.getData(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return const SizedBox();
                    } else if (snapshot.hasError) {
                      return const Center(child: ErrorScreen());
                    } else {
                      return const Center(
                        child: CupertinoActivityIndicator(
                          color: Color.fromRGBO(245, 245, 245, 1),
                          radius: 20,
                        ),
                      );
                    }
                  },
                )
              : const Text(
                  'Загрузить еще',
                  style: TextStyle(
                    fontFamily: 'Ubuntu',
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(245, 245, 245, 1),
                  ),
                ),
        ),
      ),
    );
  }

  Widget getBody() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: controller.canBeMore.value
                  ? controller.data.length + 1
                  : controller.data.length,
              padding: EdgeInsets.zero,
              itemBuilder: (cts, ind) => ind != controller.data.length
                  ? SupplierCard(index: ind)
                  : loadMoreWidget(),
            ),
          ),
        ],
      ),
    );
  }
}

class SupplierCard extends StatelessWidget {
  final controller = Get.find<MainSuppliersController>();
  final int index;
  SupplierCard({Key? key, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(
            popGesture: true,
            () => SupplierDetail(
                  data: controller.data[index],
                ));
      },
      child: Container(
        padding: const EdgeInsets.all(16),
        margin: const EdgeInsets.only(bottom: 16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: Color.fromRGBO(30, 38, 69, 1),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(15, 15, 15, 1),
              blurRadius: 5,
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              controller.data[index]['organizationName'] ?? 'Без имени',
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 22,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Количество контрактов:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              controller.data[index]['contractsTotalCount'].toString(),
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            const Text(
              'Общая вырученная сумма:',
              style: TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(245, 245, 245, 0.3),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              parsePrice(
                controller.data[index]['contractsTotalSum'].toString(),
              ),
              style: const TextStyle(
                fontFamily: 'Ubuntu',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color.fromRGBO(245, 245, 245, 1),
                letterSpacing: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

String parsePrice(String price) {
  String ed = '';
  double dPrice = double.parse(price);
  if (dPrice >= 1000000000) {
    dPrice /= 1000000000;
    ed = 'млрд.';
  } else if (dPrice >= 1000000) {
    dPrice /= 1000000;
    ed = 'млн.';
  } else if (dPrice >= 1000) {
    dPrice /= 1000;
    ed = 'тыс.';
  }
  return dPrice.toStringAsFixed(1) + ' ' + ed + ' RUB';
}
