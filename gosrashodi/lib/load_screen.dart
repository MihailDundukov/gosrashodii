import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:gosrashodi/home_screen.dart';
import 'package:rive/rive.dart' as rive;

class LoadController extends GetxController {
  @override
  void onInit() {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    Timer(const Duration(milliseconds: 3000), () {
      Get.offAll(() => const HomeScreen());
    });
    Get.put(DateController());
    Get.put(NavigatorController());
    Get.put(MainContractsController());
    Get.put(MainGrantsController());
    Get.put(MainSuppliersController());
    Get.put(MainSubsidyController());
    super.onInit();
  }
}

class LoadScreen extends StatelessWidget {
  LoadScreen({Key? key}) : super(key: key);
  final controller = Get.put(LoadController());

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color.fromRGBO(24, 15, 28, 1),
      body: Center(
        child: rive.RiveAnimation.asset(
          'assets/animations/bubble.riv',
          animations: ['Idle'],
          stateMachines: [],
        ),
      ),
    );
  }
}
