import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gosrashodi/helpers/API.dart';

import 'enums.dart';

class DateController extends GetxController {
  RxString curDate = ''.obs;
  RxString textFirstDate = ''.obs;
  RxString textSecondDate = ''.obs;
  RxList<int> firstPickerDate = <int>[0, 0, 0].obs;
  RxList<int> secondPickerDate = <int>[0, 0, 0].obs;

  void setFirstDate(DateTime newDate) {
    List<String> date = newDate.toString().substring(0, 10).split('-');
    textFirstDate.value = date[2] + '.' + date[1] + '.' + date[0].toString();
    firstPickerDate[0] = int.parse(date[0]);
    firstPickerDate[1] = int.parse(date[1]);
    firstPickerDate[2] = int.parse(date[2]);
    update();
  }

  void setSecondDate(DateTime newDate) {
    List<String> date = newDate.toString().substring(0, 10).split('-');
    textSecondDate.value = date[2] + '.' + date[1] + '.' + date[0].toString();
    secondPickerDate[0] = int.parse(date[0]);
    secondPickerDate[1] = int.parse(date[1]);
    secondPickerDate[2] = int.parse(date[2]);
    update();
  }

  @override
  void onInit() {
    List<String> date = DateTime.now().toString().substring(0, 10).split('-');
    int year = int.parse(date[0]);
    curDate.value = date[2] + '.' + date[1] + '.' + date[0];
    textFirstDate.value = date[2] + '.' + date[1] + '.' + (year - 1).toString();
    textSecondDate.value = date[2] + '.' + date[1] + '.' + date[0];
    firstPickerDate[0] = year - 1;
    firstPickerDate[1] = int.parse(date[1]);
    firstPickerDate[2] = int.parse(date[2]);
    secondPickerDate[0] = year;
    secondPickerDate[1] = int.parse(date[1]);
    secondPickerDate[2] = int.parse(date[2]);
    super.onInit();
  }
}

class NavigatorController extends GetxController {
  MyScreen? type;

  @override
  void onInit() {
    type = MyScreen.start;
    super.onInit();
  }
}

class MainContractsController extends GetxController {
  final controllerDate = Get.find<DateController>();
  RxList<dynamic> data = [].obs;
  int total = 0;
  int curPage = 1;
  int pegesSelected = 0;
  RxBool canBeMore = true.obs;
  RxBool priceMax = false.obs;
  RxBool priceMin = false.obs;
  RxBool countMax = false.obs;
  RxBool countMin = false.obs;

  getData() async {
    final answ = await getContracts(controllerDate.textFirstDate.value,
        controllerDate.textSecondDate.value, curPage);
    if (answ!.isNotEmpty) {
      for (var elem in answ) {
        data.add(elem);
      }
    }
    sortData();
    return data;
  }

  reload() {
    total = 0;
    curPage = 1;
    pegesSelected = 0;
    canBeMore.value = true;
    data.clear();
    update();
  }

  sortData() {
    if (priceMax.value) {
      data.sort((a, b) {
        var aData = num.parse(a['price'].toString());
        var bData = num.parse(b['price'].toString());
        return -aData.compareTo(bData);
      });
    } else if (priceMin.value) {
      data.sort((a, b) {
        var aData = num.parse(a['price'].toString());
        var bData = num.parse(b['price'].toString());
        return aData.compareTo(bData);
      });
    } else if (countMax.value) {
      data.sort((a, b) {
        var aData = a['products'].length;
        var bData = b['products'].length;
        return -aData.compareTo(bData);
      });
    } else if (countMin.value) {
      data.sort((a, b) {
        var aData = a['products'].length;
        var bData = b['products'].length;
        return aData.compareTo(bData);
      });
    }
  }

  String getAllPrice() {
    double sum = 0;
    for (var elem in data) {
      sum += elem['price'];
    }
    return sum.toString();
  }
}

class MainGrantsController extends GetxController {
  final controllerDate = Get.find<DateController>();
  RxList<dynamic> data = [].obs;
  int total = 0;
  int curPage = 1;
  int pegesSelected = 0;
  RxBool canBeMore = true.obs;

  reload() {
    total = 0;
    curPage = 1;
    pegesSelected = 0;
    canBeMore.value = true;
    data.clear();
    update();
  }

  getData() async {
    final answ = await getGrants(controllerDate.textFirstDate.value,
        controllerDate.textSecondDate.value, curPage);
    if (answ!.isNotEmpty) {
      for (var elem in answ) {
        data.add(elem);
      }
    }
    return data;
  }
}

class MainSuppliersController extends GetxController {
  RxList<dynamic> data = [].obs;
  int total = 0;
  int curPage = 1;
  int pegesSelected = 0;
  RxString region = '76'.obs;
  late TextEditingController textController;
  RxBool canBeMore = true.obs;

  @override
  void onInit() {
    textController = TextEditingController(text: region.value);
    super.onInit();
  }

  reload() {
    total = 0;
    curPage = 1;
    pegesSelected = 0;
    canBeMore.value = true;
    data.clear();
    update();
  }

  getData() async {
    final answ = await getSuppliers(region.value, curPage);
    if (answ!.isNotEmpty) {
      for (var elem in answ) {
        data.add(elem);
      }
    }
    return data;
  }
}

class MainSubsidyController extends GetxController {
  final controllerDate = Get.find<DateController>();
  RxList<dynamic> data = [].obs;
  int total = 0;
  int curPage = 1;
  int pegesSelected = 0;
  RxBool canBeMore = true.obs;
  RxBool priceMax = false.obs;
  RxBool priceMin = false.obs;
  RxBool countMax = false.obs;
  RxBool countMin = false.obs;

  getData() async {
    final answ = await getSubsidies(controllerDate.textFirstDate.value,
        controllerDate.textSecondDate.value, curPage);
    if (answ!.isNotEmpty) {
      for (var elem in answ) {
        data.add(elem);
      }
    }
    return data;
  }

  reload() {
    total = 0;
    curPage = 1;
    pegesSelected = 0;
    canBeMore.value = true;
    data.clear();
    update();
  }
}
