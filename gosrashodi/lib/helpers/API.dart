// ignore_for_file: file_names

import 'dart:convert' as convert;
import 'package:get/get.dart';
import 'package:gosrashodi/helpers/controllers.dart';
import 'package:http/http.dart' as http;

Future<List<dynamic>?> getContracts(
    String fDate, String sDate, int page) async {
  final controller = Get.find<MainContractsController>();
  List<dynamic> resp = [];
  int total;
  Uri url = Uri.parse(
      'http://openapi.clearspending.ru/restapi/v3/contracts/search/?daterange=$fDate-$sDate&page=$page');
  var response = await http.get(url);
  if (response.body == "Data not found.") {
    return [];
  }
  var jsonResponse = convert.jsonDecode(response.body) as Map<String, dynamic>;
  total = jsonResponse['contracts']['total'];
  controller.pegesSelected +=
      int.parse(jsonResponse['contracts']['perpage'].toString());
  for (var elem in jsonResponse['contracts']['data']) {
    resp.add(elem);
  }
  if (controller.pegesSelected < total) {
    controller.curPage++;
    controller.canBeMore.value = true;
  } else {
    controller.canBeMore.value = false;
  }
  return resp;
}

Future<List<dynamic>?> getGrants(String fDate, String sDate, int page) async {
  List<dynamic> resp = [];
  int total;
  final controller = Get.find<MainGrantsController>();

  Uri url = Uri.parse(
      'http://openapi.clearspending.ru/restapi/v3/grants/search/?daterange=$fDate-$sDate&page=$page');
  var response = await http.get(url);
  if (response.body == "Data not found.") {
    return [];
  }
  var jsonResponse = convert.jsonDecode(response.body) as Map<String, dynamic>;
  total = jsonResponse['grants']['total'];
  controller.pegesSelected +=
      int.parse(jsonResponse['grants']['perpage'].toString());
  for (var elem in jsonResponse['grants']['data']) {
    resp.add(elem);
  }
  if (controller.pegesSelected < total) {
    controller.curPage++;
    controller.canBeMore.value = true;
  } else {
    controller.canBeMore.value = false;
  }
  return resp;
}

Future<List<dynamic>?> getSuppliers(String region, int page) async {
  List<dynamic> resp = [];
  int total;
  final controller = Get.find<MainSuppliersController>();

  Uri url = Uri.parse(
      'http://openapi.clearspending.ru/restapi/v3/suppliers/search/?regioncode=$region&page=$page');
  var response = await http.get(url);
  if (response.body == "Data not found.") {
    return [];
  }
  var jsonResponse = convert.jsonDecode(response.body) as Map<String, dynamic>;
  total = jsonResponse['suppliers']['total'];
  controller.pegesSelected +=
      int.parse(jsonResponse['suppliers']['perpage'].toString());
  for (var elem in jsonResponse['suppliers']['data']) {
    resp.add(elem);
  }
  if (controller.pegesSelected < total) {
    controller.curPage++;
    controller.canBeMore.value = true;
  } else {
    controller.canBeMore.value = false;
  }
  return resp;
}

Future<List<dynamic>?> getSubsidies(
    String fDate, String sDate, int page) async {
  List<String> helpF = fDate.split('.');
  List<String> helpS = sDate.split('.');
  String frtDate = helpF[2]+'-'+helpF[1]+'-'+helpF[0];
  String sndDate = helpS[2]+'-'+helpS[1]+'-'+helpS[0];
  final controller = Get.find<MainContractsController>();
  List<dynamic> resp = [];
  int total;
  Uri url = Uri.parse(
      'https://api.sub.clearspending.ru/v1/subsidy/?info_dateAgreem=$frtDate:$sndDate&page=$page');
  var response = await http.get(url);
  if (response.body == "Data not found.") {
    return [];
  }
  var jsonResponse = convert.jsonDecode(response.body) as Map<String, dynamic>;
  total = jsonResponse['total'];
  controller.pegesSelected +=
      int.parse(jsonResponse['page'].toString());
  for (var elem in jsonResponse['items']) {
    resp.add(elem);
  }
  if (controller.pegesSelected < total) {
    controller.curPage++;
    controller.canBeMore.value = true;
  } else {
    controller.canBeMore.value = false;
  }
  return resp;
}
